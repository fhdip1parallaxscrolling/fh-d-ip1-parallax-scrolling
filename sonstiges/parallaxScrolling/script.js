var scrollPosition = window.pageYOffset;

//Elemente selektieren
var e1_1 = document.getElementById("E1_1");
var e1_2 = document.getElementById("E1_2");
var e1_3 = document.getElementById("E1_3");
var e1_4 = document.getElementById("E1_4");
var e1_5 = document.getElementById("E1_5");
var e1_6 = document.getElementById("E1_6");
var e1_7 = document.getElementById("E1_7");
var e1_8 = document.getElementById("E1_8");
var e1_9 = document.getElementById("E1_9");

//Entfernung bestimmen (von oben)
var e1_1Entfernung = Math.floor((Math.random() * 650) + 100);
var e1_2Entfernung = Math.floor((Math.random() * 650) + 100);
var e1_3Entfernung = Math.floor((Math.random() * 650) + 100);
var e1_4Entfernung = Math.floor((Math.random() * 650) + 100);
var e1_5Entfernung = Math.floor((Math.random() * 650) + 100);
var e1_6Entfernung = Math.floor((Math.random() * 650) + 100);
var e1_7Entfernung = Math.floor((Math.random() * 650) + 100);
var e1_8Entfernung = Math.floor((Math.random() * 650) + 100);
var e1_9Entfernung = Math.floor((Math.random() * 650) + 100);

e1_1.style.top = e1_1Entfernung + "px";
e1_2.style.top = e1_2Entfernung + "px";
e1_3.style.top = e1_3Entfernung + "px";
e1_4.style.top = e1_4Entfernung + "px";
e1_5.style.top = e1_5Entfernung + "px";
e1_6.style.top = e1_6Entfernung + "px";
e1_7.style.top = e1_7Entfernung + "px";
e1_8.style.top = e1_8Entfernung + "px";
e1_9.style.top = e1_9Entfernung + "px";

e1_1.style.left = Math.floor((Math.random() * 1000) + 10) + "px";
e1_2.style.left = Math.floor((Math.random() * 1000) + 10) + "px";
e1_3.style.left = Math.floor((Math.random() * 1000) + 10) + "px";
e1_4.style.left = Math.floor((Math.random() * 1000) + 10) + "px";
e1_5.style.left = Math.floor((Math.random() * 1000) + 10) + "px";
e1_6.style.left = Math.floor((Math.random() * 1000) + 10) + "px";
e1_7.style.left = Math.floor((Math.random() * 1000) + 10) + "px";
e1_8.style.left = Math.floor((Math.random() * 1000) + 10) + "px";
e1_9.style.left = Math.floor((Math.random() * 1000) + 10) + "px";

//Größe bestimmen
e1_1.style.height = e1_1.style.width = Math.floor((Math.random() * 25) + 10) + "px";
e1_2.style.height = e1_2.style.width = Math.floor((Math.random() * 25) + 10) + "px";
e1_3.style.height = e1_3.style.width = Math.floor((Math.random() * 25) + 10) + "px";
e1_4.style.height = e1_4.style.width = Math.floor((Math.random() * 25) + 10) + "px";
e1_5.style.height = e1_5.style.width = Math.floor((Math.random() * 25) + 10) + "px";
e1_6.style.height = e1_6.style.width = Math.floor((Math.random() * 25) + 10) + "px";
e1_7.style.height = e1_7.style.width = Math.floor((Math.random() * 25) + 10) + "px";
e1_8.style.height = e1_8.style.width = Math.floor((Math.random() * 25) + 10) + "px";
e1_9.style.height = e1_9.style.width = Math.floor((Math.random() * 25) + 10) + "px";




window.onscroll = function () {
    console.log("SCROLL!!!" +this.pageYOffset);

    if (this.pageYOffset <= scrollPosition) {
        e1_1Entfernung -= 2;
        e1_2Entfernung += 3;
        e1_3Entfernung -= 4;
        e1_4Entfernung += 5;
        e1_5Entfernung -= 6;
        e1_6Entfernung += 7;
        e1_7Entfernung -= 8;
        e1_8Entfernung += 9;
        e1_9Entfernung -= 10;
    }
    else {
        e1_1Entfernung += 2;
        e1_2Entfernung -= 3;
        e1_3Entfernung += 4;
        e1_4Entfernung -= 5;
        e1_5Entfernung += 6;
        e1_6Entfernung -= 7;
        e1_7Entfernung += 8;
        e1_8Entfernung -= 9;
        e1_9Entfernung += 10;
    }

    scrollPosition = this.pageYOffset;
    e1_1.style.top = e1_1Entfernung + "px";
    e1_2.style.top = e1_2Entfernung + "px";
    e1_3.style.top = e1_3Entfernung + "px";
    e1_4.style.top = e1_4Entfernung + "px";
    e1_5.style.top = e1_5Entfernung + "px";
    e1_6.style.top = e1_6Entfernung + "px";
    e1_7.style.top = e1_7Entfernung + "px";
    e1_8.style.top = e1_8Entfernung + "px";
    e1_9.style.top = e1_9Entfernung + "px";

}