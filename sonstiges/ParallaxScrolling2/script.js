(function () {

    //Anfängliche Scrollposition ermitteln
    var scrollPosition = window.pageYOffset;

    //Elemente selektieren
    var h0 = document.getElementById("hintergrund0");
    var h1 = document.getElementById("hintergrund1");
    var h2 = document.getElementById("hintergrund2");
    var h3 = document.getElementById("hintergrund3");

    //Anfängliche top-Position initialisieren
    var top = 0;

    function parallax() {

        //Herunter scrollen
        if (this.pageYOffset > scrollPosition) {
            top += 1;
        }
        //Hinauf scrollen
        else {
            top -= 1;
        }

        //Elemente neu positionieren
        h0.style.top = top + "px";
        h1.style.top = top + "px";
        h2.style.top = top + "px";
        h3.style.top = top + "px";

        //aktuelle Scroll-Position speichern
        scrollPosition = this.pageYOffset;

    }

    addEventListener("scroll", parallax, false)

})();